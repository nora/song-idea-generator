use std::fmt::Display;

pub fn display_list(list: &[impl Display]) -> String {
    match list.len() {
        0 =>  "".into(),
        1 => format!("{}", list[0]),
        2 => format!("{} and {}", list[0], list[1]),
        _ => format!("{}and {}", display_list_commas(&list[0..list.len()-1]), list[list.len()-1])
    }
}

fn display_list_commas(list: &[impl Display]) -> String {
    let mut string = String::new();
    for item in list {
        string.push_str(&format!("{}, ", item));
    }
    string
}

#[test]
fn single_item() {
    let list = ["red"];
    assert_eq!(&display_list(&list), "red");
}

#[test]
fn two_items() {
    let list = ["red", "green"];
    assert_eq!(&display_list(&list), "red and green");
}

#[test]
fn three_items() {
    let list = ["red", "green", "blue"];
    assert_eq!(&display_list(&list), "red, green, and blue");
}

#[test]
fn four_items() {
    let list = ["red", "yellow", "green", "blue"];
    assert_eq!(&display_list(&list), "red, yellow, green, and blue");
}

#[test]
fn many_items() {
    let list = ["red", "orange" , "yellow", "green", "blue", "purple"];
    assert_eq!(&display_list(&list), "red, orange, yellow, green, blue, and purple");
}
