use serde::{Deserialize, Serialize};

use derive_more::Display;
use rand::distributions::{Distribution, Standard};
use rand::Rng;

use crate::displayutil::display_list;

#[derive(Debug, PartialEq, Copy, Clone, Display, Serialize, Deserialize)]
pub enum Effect {
    #[display(fmt = "delay")]
    Delay,
    #[display(fmt = "reverb")]
    Reverb,
    #[display(fmt = "lofi")]
    Lofi,
    #[display(fmt = "drive")]
    Drive,
    #[display(fmt = "chorus")]
    Chorus,
    #[display(fmt = "tremolo")]
    Tremolo,
    #[display(fmt = "distortion")]
    Distortion,
    #[display(fmt = "flanger")]
    Flanger,
    #[display(fmt = "phaser")]
    Phaser,
    #[display(fmt = "compressor")]
    Compressor,
    #[display(fmt = "sustain")]
    Sustain,
    #[display(fmt = "bitcrusher")]
    Bitcrusher,
    #[display(fmt = "wavefolder")]
    Wavefolder,
    #[display(fmt = "ring modulator")]
    RingMod,
    #[display(fmt = "wah")]
    Wah,
}

impl Distribution<Effect> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Effect {
        let index: u8 = rng.gen_range(0..15);
        use Effect::*;
        match index {
            0 => Delay,
            1 => Reverb,
            2 => Lofi,
            3 => Drive,
            4 => Chorus,
            5 => Tremolo,
            6 => Distortion,
            7 => Flanger,
            8 => Phaser,
            9 => Compressor,
            10 => Sustain,
            11 => Bitcrusher,
            12 => Wavefolder,
            13 => RingMod,
            14 => Wah,
            _ => unreachable!(),
        }
    }
}

pub fn generate_effects() -> Vec<Effect> {
    let mut vec = Vec::<Effect>::new();
    while rand::random() {
        vec.push(rand::random())
    }
    vec
}

pub fn display_effects(effects: &[Effect]) -> String {
    if effects.is_empty() {
        "clean".into()
    } else {
        format!("through {}", display_list(effects))
    }
}
