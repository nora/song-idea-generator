use rand::prelude::SliceRandom;
use serde::{Deserialize, Serialize};

use crate::displayutil::display_list;
use derive_more::Display;
use rand::distributions::{Distribution, Standard};
use rand::Rng;
#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, Display)]
pub enum VoiceAdjective {
    Glassy,
    Wide,
    Glitchy,
    Aggressive,
    Warm,
    Thick,
    Clean,
    Hard,
    Wet,
    Dry,
    Thin,
    Tinny,
    Splashy,
    Heavy,
    Light,
    Dark,
    Smooth,
    Plucky,
    Soft,
    Droning,
    Digital,
    Squishy,
    Analogue,
    Round,
    Sharp,
    Crunchy,
    Aliased,
    Punchy,
    Intense,
    Mild,
    Calm,
    Sonorous,
    Resonant,
    Buzzy,
    Dampened,
    Subdued,
    Rampant,
    Hot,
    Sweaty,
    Flooded,
    Plonky,
    Yoinky,
    Sploinky,
    Boofy,
    Middy,
    Grimy,
    Ugly,
    Soothing,
    Skittery,
    Harsh,
    Alien,
    Bubbly,
    Airy,
    Subterranean,
    Claustrophobic,
    Spacious,
    Ringing,
    Clicky,
    Acidic,
    Solid,
    Ethereal,
    Massive,
    Distant,
    Muffled,
    Garbled,
    Degraded,
    BlownOut,
    Sizzling,
    #[display(fmt = "In-your-face")]
    Inyouface,
    #[display(fmt = "Way Huge")]
    Wayhuge,
    ElectroHarmonic,
    Surfy,
    Nostalgic,
    Ancient,
    Angular,
    Fried,
    Ineffable,
    Ramshackle,
    Mercurial,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Voice {
    adjective: VoiceAdjective,
    voice: String,
}

impl Distribution<VoiceAdjective> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> VoiceAdjective {
        use VoiceAdjective::*;
        let values = [
            Glassy,
            Wide,
            Glitchy,
            Aggressive,
            Warm,
            Thick,
            Clean,
            Hard,
            Wet,
            Dry,
            Thin,
            Tinny,
            Splashy,
            Heavy,
            Light,
            Dark,
            Smooth,
            Plucky,
            Soft,
            Droning,
            Digital,
            Squishy,
            Analogue,
            Round,
            Sharp,
            Crunchy,
            Aliased,
            Punchy,
            Intense,
            Mild,
            Calm,
            Sonorous,
            Resonant,
            Buzzy,
            Dampened,
            Subdued,
            Rampant,
            Hot,
            Sweaty,
            Flooded,
            Plonky,
            Yoinky,
            Sploinky,
            Boofy,
            Middy,
            Grimy,
            Ugly,
            Soothing,
            Skittery,
            Harsh,
            Alien,
            Bubbly,
            Airy,
            Subterranean,
            Claustrophobic,
            Spacious,
            Ringing,
            Clicky,
            Acidic,
            Solid,
            Ethereal,
            Massive,
            Distant,
            Muffled,
            Garbled,
            Degraded,
            BlownOut,
            Sizzling,
            Inyouface,
            Wayhuge,
            ElectroHarmonic,
            Surfy,
            Nostalgic,
            Ancient,
            Angular,
            Fried,
            Ineffable,
            Ramshackle,
            Mercurial,
        ];
        values.choose(rng).unwrap().clone()
    }
}

pub fn generate_voice_adjective(voice: &'static str) -> Option<Voice> {
    if rand::random() {
        None
    } else {
        Some(Voice {
            adjective: rand::random(),
            voice: voice.into(),
        })
    }
}

pub fn display_voices(list: &[Voice]) -> String {
    let voices_formatted: Vec<_> = list
        .iter()
        .map(|voice| format!("{} {}", voice.adjective, voice.voice).to_lowercase())
        .collect();
    display_list(&voices_formatted)
}
