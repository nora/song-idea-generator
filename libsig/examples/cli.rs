use libsig;
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name="sigcli", about="generate ideas for songs, tracks, and sounds")]
struct CliOptions {
    /// Don't suggest percussion elements
    #[structopt(short, long)]
    ambient: bool,
    /// The number of suggestions to generate
    #[structopt(default_value="1")]
    suggestions: usize
}

fn main() {
    let opt = CliOptions::from_args();
    
    for _ in 0..opt.suggestions {
        let idea = if opt.ambient {
            libsig::SongIdea::generate_ambient()
        } else {
            libsig::SongIdea::generate()
        };
        println!("{}", idea)
    }
}