use maud::{html, DOCTYPE};
use libsig::SongIdea;

pub fn main_page_template(idea: Option<SongIdea>, base_url: &str) -> String {
    let html = html! {
        (DOCTYPE)
        head {
            link rel="stylesheet" href="style.css";
            base href=(base_url);
        }
        body {
            h1 { "Song Idea Generator" } 
            (match idea {
                Some(idea) => html!{ 
                    p { "Why don't you make a song like this: " }
                    p.content { (idea) }
                },
                None => html! { p { "Generate a new idea with the links below!" } p.content {} }
            })
            footer {
                p {
                    a href="./generate" { "generate new" }
                    " | "
                    a href="./generate_ambient" { "generate new (ambient)" }
                }
                p {
                    a href="http://nora.codes" { "my blog" }
                    " | "
                    a href="https://git.nora.codes/nora/song-idea-generator" { "code" }
                }
            }
        }
    };
    html.into()
}