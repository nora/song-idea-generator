# Song Idea Generator

Consisting of a Rust library for generating song ideas `libsig`,
a command line tool for calling `libsig`,
and a web interface with a JSON API for getting structured results from `libsig`.

![an example of sigweb in use](sigweb.png)

## Usage

The primary type of `libsig` is the `SongIdea`. You can generate human-readable representations with `Display` (so, `println!("{}", my_song_idea)`).

For ease of use there is a command line tool called `sigcli`:

```
$ cargo run --quiet --example cli -- 5
skittery kick, dampened hat, and calm tom at 254 bpm, with no swing, through bitcrusher, reverb, phaser, distortion, and wah
skittery bass, smooth pad, massive snare, smooth tom, and digital cymbal at 33 bpm, with heavy swing, clean
splashy lead and harsh bass at 39 bpm, with no swing, through compressor
buzzy pad, glassy vocals, plonky kick, plucky snare, and dry tom at 63 bpm, with heavy swing, through ring modulator
surfy lead, blownout pad, light vocals, acidic snare, sonorous hat, and digital tom at 33 bpm, with no swing, through lofi
```

It also takes a parameter `--ambient` to avoid suggesting percussion parts.

`sigweb` is a very basic Rocket app. I suggest building it with `x86_64-unknown-linux-musl`. Once built take the `sigweb` binary and put it in a folder with an appropriate `Rocket.toml`. You can set a base URL with the environment variable `SIGWEB_BASE_URL`. See the provided `sigweb.service` for a sample SystemD unit.

It provides a JSON API with the endpoints `/api/v1/{generate, generate_ambient}`.